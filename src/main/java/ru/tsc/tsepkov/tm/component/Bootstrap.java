package ru.tsc.tsepkov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.tsepkov.tm.api.repository.ICommandRepository;
import ru.tsc.tsepkov.tm.api.repository.IProjectRepository;
import ru.tsc.tsepkov.tm.api.repository.ITaskRepository;
import ru.tsc.tsepkov.tm.api.repository.IUserRepository;
import ru.tsc.tsepkov.tm.api.service.*;
import ru.tsc.tsepkov.tm.command.AbstractCommand;
import ru.tsc.tsepkov.tm.enumerated.Role;
import ru.tsc.tsepkov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.tsepkov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.tsepkov.tm.model.User;
import ru.tsc.tsepkov.tm.repository.CommandRepository;
import ru.tsc.tsepkov.tm.repository.ProjectRepository;
import ru.tsc.tsepkov.tm.repository.TaskRepository;
import ru.tsc.tsepkov.tm.repository.UserRepository;
import ru.tsc.tsepkov.tm.service.*;
import ru.tsc.tsepkov.tm.util.SystemUtil;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.tsepkov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }


    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(@Nullable final String[] args) {
        processArguments(args);
        initPID();
        initDemoData();
        initLogger();
        System.out.println("** WELCOME TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final RuntimeException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    private void initDemoData() {
        @Nullable final User user369 = userService.create("user369", "user369", "user369@mail.ru");
        @Nullable final User user367 = userService.create("user367", "user367", "user367@mail.ru");
        @Nullable final User admin = userService.create("admin", "admin", Role.ADMIN);
        if (user369 != null && user367 != null && admin != null) {
            projectService.create(user369.getId(), "PROJECT A", "123");
            projectService.create(user369.getId(), "STAR", "new");
            projectService.create(user369.getId(), "PROJECT B", "12345");
            projectService.create(user369.getId(), "CAR", "new");
            projectService.create(user369.getId(), "PROJECT C", "1234567890");
            projectService.create(admin.getId(), "PROJECT D", "HELLO");
            projectService.create(admin.getId(), "PROJECT E", "WORLD");

            taskService.create(user369.getId(), "TASK A", "ASD");
            taskService.create(user369.getId(), "TASK B", "QWE");
            taskService.create(user367.getId(), "TASK C", "ZXC");
            taskService.create(user367.getId(), "TASK D", "UIO");
        }
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

}
