package ru.tsc.tsepkov.tm.exception.field;

public final class UserEmptyException extends AbstractFieldException {

    public UserEmptyException() {
        super("Error! User is empty...");
    }

}
